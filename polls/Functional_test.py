from __future__ import absolute_import
from unittest import TestCase

# import sys
from selenium import webdriver
from selenium.webdriver.common.by import By


class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome("/Users/hawer.torres/Documents/chromedriver")
        self.browser.set_window_size(1024, 768)
        self.browser.implicitly_wait(5000)

    def tearDown(self):
        self.browser.quit()

    def test_1_title(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

